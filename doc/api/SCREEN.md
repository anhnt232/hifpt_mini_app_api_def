# **SCREEN​**

## **showLoading(...args)​**

### **1. Function introduction**

show Loading

### **2. Function request & response params**

#### **Request**

Params | Data type | Description
-------|-----------|------------
args   | array     | args

### **3. Sample Request & Response**

#### **Request**

```js
MiniApi.showLoading([""])
```

#### **Response**

void

## **hideLoading(...args)​**

### **1. Function introduction**

hide Loading.

### **2. Function request & response params**

#### **Request**

Params | Data type | Description
-------|-----------|------------
args   | array     | args

### **3. Sample Request & Response**

#### **Request**

```js
MiniApi.hideLoading()
```

#### **Response**

void
