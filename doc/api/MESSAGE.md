# **MESSAGE, DIALOG​**

## **showToast(...args)​**

### **1. Function introduction**

show Toast

### **2. Function request & response params**

#### **Request**

Params | Data type | Required | Description
-------|-----------|----------|------------
object | object    | YES      | Object contains field like description, type, duration
description| string| YES      | Toast description
type   | string    | YES      | Toast type
duration| number   | YES      | Toast duration

#### **Reponse**

Show toast at the end of the screen

### **3. Sample Request & Reponse**

#### **Request**

```js
MiniAPI.showToast({
      description: "Successfully",
      type: "success",
      duration: 2000
});
```

#### **Response**

![Image](mini-app_func-catalog_showToast-2.png)

## **hideToast(...args)​**

### **1. Function introduction**

Hide toast

### **2. Function request & response params**

#### **Request**
Params | Data type | Required | Description
-------|-----------|----------|------------
TODO |

### **3. Sample Request & Response**

#### **Request**

```js
MiniAPI.hideToast();
```

## **showAlert(title, message, buttonTitles, callback)​**

### **1. Function introduction**

show dialog alert

### **2. Function request & response params**

#### **Request**

Params | Data type | Required | Description
-------|-----------|----------|------------
title  | string	   | YES      | title
message| string	   | YES      | message
buttonTitles| array| YES      | button titles
callback| function | YES      | return index of CTAs of dialog which be clicked

### **3. Sample Request & Response**

#### **Request**

``` js
MiniApi.showAlert(
                "hello",
                "hello world",
                ["OK", "Cancel"],
                (data) => {
                    console.log(data);
                }
            );
```

#### **Response**

Params | Data type | Description
-------|-----------|------------
data   | number    | return 0 or 1 based on the action chosen

## **showAction(title, buttonTitles, callback)​**

### **1. Function introduction**

show bottom sheet to choose action

### **2. Function request & response params**

#### **Request**

Params | Data type | Description
-------|-----------|------------
title  | string    | title
buttonTitles| array| button titles
callback| function | return index of CTAs of bottomSheet which be clicked

### **3. Sample Request & Response**

#### **Request**

```js
MiniApi.showAction(
                "hello",
                ["OK", "Cancel"],
                (data) => {
                    console.log(data);
                }
            );
```

#### **Response**

Params | Data type | Description
-------|-----------|------------
data   | number    | return 0 or 1 based on the action chosen

