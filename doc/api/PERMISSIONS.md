# **PERMISSIONS**

## **requestPermission(permission, callback)**​

### **1. Function introduction**

Request permissions on the device such as camera, location,...

### **2. Function request & response params**

#### **Request**

Params	Data type	Required	Description
permission	string		"camera" | "photo" | "storage" | "location" | "contacts"
callback	callback		callback
### **3. Sample Request & Response**

#### **Request**

```js
MiniApi.requestPermission('photo', (data) => {
        console.log(data);
    })
```
#### **Response**

Params | Data type | Description
-------|-----------|------------
data   | string    | 'granted' if allow, 'denied' if not allow, 'unavailable' if the permission is not exist

## **checkPermission(permission, callback)​**

### **1. Function introduction**

Check permission has granted

### **2. Function request & response params**

#### **Request**

Params | Data type | Required | Description
-------|-----------|----------|------------
permission| string | YES | "camera" , "photo" , "storage" , "location" , "contacts"
callback| function| YES| callback

### **3. Sample Request & Response**

#### **Request**

```js
MiniApi.checkPermission(‘camera’, (data) => {
    console.log(data);
})
```
#### **Response**

Params | Data type | Description
-------|-----------|------------
data| string| 'granted' if allow, 'denied' if not allow, 'unavailable' if the permission is not exist


