# **OLD API App-webview**

## **callbackToApp(action)**

webview callback to app with action name

#### **Request**

Params | Data type | Required | Description
-------|-----------|----------|------------
action | object    | YES      | webview callback to app with action

#### **Action object**

Field | Data type| Required| Description
------|----------|---------|------------
action| string   | YES     | Action name needs to be processed by the main-app. posible values: "getToken", "getTokenWithDeviceId", "tokenExpire", "affiliate_marketing", "registerInternet", "logout", "current_payment", "callContract","contractInfo", "callModem", "getLocation", "shareLink", "share", "download", "callCamera", "addressBook", "addressBookHDI", "PhoneBook", "takePhoto"
optional extra param| depends on the action type

#### **Action descriptions**

Action name | Description | Simple
------------|-------------|-------
getToken| Get app's access token| {"action": "getToken"}
getTokenWithDeviceId || {"action":"getTokenWithDeviceId"}
tokenExpire | |{"action": "tokenExpire"}
affiliate_marketing| |{"action" : "affiliate_marketing"}
registerInternet| |{"action" : "registerInternet"}
logout| |{"action" : "logout"}
contractInfo| |{"action" : "contractInfo"}
getLocation| | {"action": "getLocation"}
Home| | {"action":"Home"}
PhoneBook||{"action":"PhoneBook"}
addressBook || {"action":"addressBook"}
addressBookHDI || {"action":"addressBookHDI","dataBackup":""}

#### **Action with extra params**

Action name | Description | Extra params |Type | Simple
------------|-------------|------------|-------|----
callCamera  |  Call Camera| merchant_length| string | {"action": "callCamera", "merchant_length": "8"}
current_payment| | contractNo | string|{"action": "current_payment","contractNo": "SGJ111111", "contractId": "123456", "service_type": "FTEL"}
callContract|| contractNo | string| {"action": "callContract", "contractNo":"SGJ111111","contractId": "123456"}
callModem || contractNo | string |{"action": "callModem", "contractNo":"HCM.DL.0387854404","contractId": "123456"}
shareLink || link| string|{"action": "shareLink", "link": "https://www.youtube.com/watch?v=YniCNsMaGUo&ab_channel=nao"}
takePhoto| | typeDocument| string : "cmnd_before", "cmnd_after", "store_1", "store_2", "store_3" | {"action": "takePhoto", "typeDocument": "cmnd_before"}
download | | url |string | {"action": "download", "url":"https://hdimedia.hdinsurance.com.vn/f/f023393209824b69a6645902573a3322", "type": "pdf"}
share | | type | string: "url", "text", "image" |{"action": "share", "type": "url","title": "App HiFPT", "linkShare":"https://staging-hi.fpt.vn/shopping/vne/webview/team-information"}
share | | type | string: "url", "text", "image" | {"action": "share", "type": "text","title": "App HiFPT", "content":"Nhập nội dung cần thiết"}
share | | type | string: "url", "text", "image" |{"action": "share", "type": "image", "title": "App HiFPT", "image":"base64"}

### **Response**

TODO

## **callDownloadToApp(action)**

webview call action download from app

#### **Request**

Params | Data type | Required | Description
-------|-----------|----------|------------
action | object    | YES      | webview callback to app with action

### **action object**

Field | Data type|Description |simple
------|----------|------------|-----
action| string | download action| "download"
uril  | string | url | "https://hdimedia.hdinsurance.com.vn/f/f023393209824b69a6645902573a3322"
type  | string | type | "pdf"

### Simple input

```json
{
    "action": "download",
    "url": "https://hdimedia.hdinsurance.com.vn/f/f023393209824b69a6645902573a3322",
    "type": "pdf"
}
```

## **surveyAction(action)**

webview call app to return home screen

#### **Request**

Params | Data type | Required | Description
-------|-----------|----------|------------
action | object    | YES      | webview callback to app with action

### action object

Field | Data type|Description |simple
------|----------|------------|-----
action| string | return to the app's home screen| "backHome"

### simple input

```json
{
    "action": "backHome"
}
```

## **callPaymentWebkit(data)**

Webview call the payment service frome app to pay the bill.

#### **Request**

Field | Data type|Description
------|----------|-----------
data  | data object| data object sent to app

### Data object

Field | Data type|Description |Posible values
------|----------|------------|-----
referral_code| string | |
trans_type| string | |
trans_id| string | |
service_type| string | |
total_amount| number | |
promo_code|  string | |
promo_value| number | |
items| array | array of pomotion object |

### Promotion object

Field | Data type|Description
------|----------|-----------
title | string |
amount| number |

### Simple input

```json
"data": {
  "referral_code": "0123211242",
  "trans_type": "FPTPLAY",
  "trans_id": "FPL000001",
  "service_type": "FPTPLAY",
  "total_amount": 20000,
  "promo_code": "abcd",
  "promo_value": -10000,
  "items": [{
    "title": "Hoa don dich vu FPLAY",
    "amount": 20000,
  }]
  }
```

#### **Response**

TODO


## **initServices(data)**

#### **Request**

Field | Data type|Description
------|----------|-----------
data  | data object| data object sent to app

### Data object

Field | Data type|Description| Simple
------|----------|-----------|---
token | string   | the app's access token, used for API authetification | "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJqdGkiOiI0OThlODBhNy04ZjcxLTQ1MGItODRhOC1hMjMzOTY5MTk5NjYiLCJpYXQiOjE2MzQ3ODYxMjYsImV4cCI6MTYzNDc4OTcyNn0.ZAIQDd03TXpF4gjacuajfzqXTRMeQEcWzYmpWYW3CRX7Ois5sM08yDrf8AktnDnLHOthxtlZSjpbsqeZguFw"
device_id_foxpay| string| Foxpay's device ID | "1234523532"
device_id| string| app's device ID | "1234523532"
merchant_id | string | | "001122"

#### Simple input

```json
{
    "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJqdGkiOiI0OThlODBhNy04ZjcxLTQ1MGItODRhOC1hMjMzOTY5MTk5NjYiLCJpYXQiOjE2MzQ3ODYxMjYsImV4cCI6MTYzNDc4OTcyNn0.ZAIQDd03TXpF4gjacuajfzqXTRMeQEcWzYmpWYW3CRX7Ois5sM08yDrf8AktnDnLHOthxtlZSjpbsqeZguFw",
    "device_id_foxpay": "1234523532",
    "device_id": "1234523532",
    "merchant_id": "001122"
}
```

## **resultPayment(data)**

#### **Request**

Field | Data type|Description
------|----------|-----------
data  | data object| data object sent to webview

### Data object

Field | Data type|Description| Simple
------|----------|-----------|---
order_id| string | order ID  | "FTELEC_1_5230"
trans_id| number | transaction ID| 123
contractId| string| contract ID| "123445567"

#### Simple input

```json
{
    "order_id": "FTELEC_1_5230",
    "trans_id": 123,
    "contractId": "123445567"
}
```

## **resultLocation(data)**

Return location data (lat, lon) to webview

#### **Request**

Field | Data type|Description
------|----------|-----------
data  | data object| data object sent to webview

### Data object

Field | Data type|Description| Simple
------|----------|-----------|---
lat| string | latitude  | "10.762622"
lon| string | longitude | "106.660172"

#### Simple input

```json
{
    "lat": "10.762622",
    "lon": "106.660172"
}
```

## **resultPhoto(data)**

Truyền kết quả url ảnh tới webkit

#### **Request**

Field | Data type|Description
------|----------|-----------
data  | data object| data object sent to webview

### Data object

Field | Data type|Description| Simple
------|----------|-----------|---
data| string |   | "iVBORw0K…."
typeImage| number | | "image/png"
typeDocument| string| | "cmnd_before"

#### Simple input

```json
{
    "data": "iVBORw0K….",
    "typeImage": "image/png",
    "typeDocument": "cmnd_before"
}
```

## **resultPhoto(data)**

Truyền kết quả của download file

#### **Request**

Field | Data type|Description
------|----------|-----------
data  | data object| data object sent to webview

### Data object

Field | Data type|Description| Simple
------|----------|-----------|---
status| number |   | 1
msg| string | | "Success"

#### Simple input

```json
{
    "status": 1,
    "msg": "Success"
}
```

## **resultAddress(data)**

Truyển về webkit từ layout hợp đồng

#### **Request**

Field | Data type|Description
------|----------|-----------
data  | data object| data object sent to webview

### Data object

Field | Data type|Description| Simple
------|----------|-----------|---
id| number |   |
fullname| string | |
address | string | |
phone_nb | string | |
email | string | |
type | string | |

#### Simple input

```json
{
    "id": 1,
    "fullname": "",
    "address": "",
    "phone_nb": "",
    "email": "",
    "type": ""
}
```

## **resultAddress(data)**

Truyển về webkit từ layout hợp đồng HDI	

#### **Request**

Field | Data type|Description
------|----------|-----------
data  | data object| data object sent to webview

### Data object

Field | Data type|Description| Simple
------|----------|-----------|---
TODO

#### Simple input Cá nhân

```json
{
    "id": 1,
    "fullName": "Nguyễn Văn Thuỳ",
    "fullAddress": "240 Tân Kỳ Tân Quý, Phường 10, Quận Tân Bình, TP. Hồ Chí Minh",
    "phone": "0906123456" ,
    "email": "nguyenvan_thuy@gmail.com",
    "cmnd": "215123456",
    "codeCity":  "89",
    "codeDistrict":  "892",
    "codeWard": "30601",
    "address": "240 Tân Kỳ Tân Quý",
    "idTax": "",
    "saveInfo": true,
    "addressProvinceName": "Tỉnh Thái Nguyên",
    "addressDistrictName": "Thị xã Phổ Yên",
    "addressVillageName": "Xã Hồng Tiến",
    "type": "CN"
}
```

#### Simple input Doanh nghiệp

```json
{
    "id": 1,
    "fullName": "Công ty TNHH Một Thành Viên",
    "fullAddress": "240 Tân Kỳ Tân Quý, Phường 10, Quận Tân Bình, TP. Hồ Chí Minh",
    "phone":"0906123456" ,
    "email": "nguyenvan_thuy@gmail.com",
    "cmnd":"",
    "codeCity":  "89",
    "codeDistrict":  "892",
    "codeWard": "30601",
    "address": "240 Tân Kỳ Tân Quý",
    "idTax": "1236123456",
    "saveInfo": false,
    "isDefault": "false",
    "addressProvinceName": "Tỉnh Thái Nguyên",
    "addressDistrictName": "Thị xã Phổ Yên",
    "addressVillageName": "Xã Hồng Tiến",
    "type":"CN"
}
```

### **@LinhTD21 Review these API definition, too many error on API definition, Simple not consistence, No predifine type, etc**


## **resultPhoneBook(data)**

Truyển về webkit lấy thông tin trong danh bạ

#### **Request**

Field | Data type|Description
------|----------|-----------
data  | data object| data object sent to webview

### Data object

Field | Data type|Description| Simple
------|----------|-----------|---
TODO

#### Simple input

```json
{
"data": [{
        "fullName": "Nguyễn Văn Thuỳ",
        "phone": [{
                "label": "mobile",
                "value": "0987654321"
            },
            {
                "label": "home",
                "value": "0912345678"
            }
        ],
        "email": [{
                "label": "work",
                "value": "abc@fpt.com.vn"
            },
            {
                "label": "personal",
                "value": "abc@gmail.com"
            }
        ],
        "avatar": "/9j/4AAQSkZJRgABAQIAHAAcAAD/2wBDAAMCAgMCAgMDAwMEAwMEBQgFBQQEBQoHBwYIDAoMDAsKCwsNDhIQDQ4RDgsLEBYQER…. "
    },
    {
        "fullName": "Nguyễn Văn Bê",
        "phone": [{
                "label": "mobile",
                "phone": "0987654321"
            },
            {
                "label": "home",
                "phone": "0912345678"
            }
        ],
        "email": [],
        "avatar": null
    }
]
}
```
### **TODO: Data object is too complex, Needs to be defined in small object.**


## **resultIdentityCard(data)**

TODO: Make description HERE

#### **Request**

Field | Data type|Description
------|----------|-----------
data  | data object| data object sent to webview

### Data object

Field | Data type|Description| Simple
------|----------|-----------|---
TODO

#### Simple input

```json
{
    "fullName":"Nguyễn Văn A",
    "birthDate":"12/01/1999",
    "IdentityCard":"001001001001",
    "sex":"0",
    "typeDocument":"CMND"
}
```

TO BE COMPLETED
