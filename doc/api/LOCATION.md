# **LOCATION​**

## **Old API**

### **callbackToApp(action)**

Params | Data type | Required | Description
-------|-----------|----------|------------
action | object    | YES      | webview callback to app with action

### Action object

Field | Data type| Required| Description| Simple
------|----------|---------|------------|--
action| string: "getLocation"| YES     | Get location| {"action": "getToken"}

### **resultLocation(data)**

Return location data (lat, lon) to webview

#### **Request**

Field | Data type|Description
------|----------|-----------
data  | data object| data object sent to webview

### Data object

Field | Data type|Description| Simple
------|----------|-----------|---
lat| string | latitude  | "10.762622"
lon| string | longitude | "106.660172"

#### Simple input

```json
{
    "lat": "10.762622",
    "lon": "106.660172"
}
```


## **New API**

### **getLocation(callback)​**

### **1. Function introduction**

get location of device

### **2. Function request & response params**

#### **Request**

Params | Data type | Required | Description
-------|-----------|----------|------------
callback| function| YES| return device's location
### **3. Sample Request & Response**

#### **Request**

```js
MiniApi.getLocation((data) => {
    console.log(data);
})
```
#### **Response**

```json
{
    "mocked": boolean,
    "timestamp": number,
    "coords": {
        "speed": number,
        "heading": number,
        "altitude": number,
        "accuracy": number,
        "longitude": float,
        "latitude": float
    }
}
```

Params | Data type | Description
-------|-----------|------------
data   | object    |(above code block)

