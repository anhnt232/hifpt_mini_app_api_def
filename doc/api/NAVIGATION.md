# **NAVIGATION​**

## **goHome(callback)​**

### **1. Function introduction**

Navigate to home screen

### **2. Function request & response params**

#### **Request**

Params | Data type | Required | Description
-------|-----------|----------|------------
callback| function |YES | callback

### **3. Sample Request & Response**

#### **Request**

    MiniAPI.goHome(callback);

#### **Response**

N/A

## **goBack(callback)​**

### **1. Function introduction**

Navigate to previous screen

### **2. Function request & response params**

#### **Request**

Params | Data type | Required | Description
-------|-----------|----------|------------
callback| function |YES | callback

### **3. Sample Request & Response**

#### **Request**

    MiniAPI.goBack(callback);

#### **Response**

N/A

## **openURL(callbackDeeplink)​**

### **1. Function introduction**

Open url with callback deep link

### **2. Function request & response params**

#### **Request**

Params | Data type | Required | Description
-------|-----------|----------|------------
callbackDeepLink| function |YES | callback

### **3. Sample Request & Response**

#### **Request**

    MiniAPI.openURL(callbackDeepLink);

#### **Response**

N/A

## **openWeb(params, callback)​**

### **1. Function introduction**

Open web url

### **2. Function request & response params**

#### **Request**

Params | Data type | Required | Description
-------|-----------|----------|------------
params | object | YES| {url,html,title}
callback| callback| YES| callback

### **3. Sample Request & Response**

#### **Request**
```js
MiniAPI.openWeb(
     {
       url: "https://www.google.com",
       title: "google"
     },
     callback
   );
```
#### **Response**

N/A

## **dismissAll(callback)​**

### **1. Function introduction**

Dismiss all screen

### **2. Function request & response params**

#### **Request**

Params | Data type | Required | Description
-------|-----------|----------|------------
callback| function |YES | callback

### **3. Sample Request & Response**

#### **Request**

    MiniAPI.dismissAll(callback);

#### **Response**

N/A

## **dismiss(result, callback)​**

### **1. Function introduction**

Dismiss current screen and return result for previous route

### **2. Function request & response params**

#### **Request**

Params | Data type | Required | Description
-------|-----------|----------|------------
result | object | YES |result
callback| function |YES | callback

### **3. Sample Request & Response**

#### **Request**

    MiniAPI.dismiss({resultCode:"001"},callback);

#### **Response**

N/A
