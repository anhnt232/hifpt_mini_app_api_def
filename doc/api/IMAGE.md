# **IMAGE​**

## **getBase64FromUrl(url, callback)​**

### **1. Function introduction**

Convert image from url to base64 string

### **2. Function request & response params**

#### **Request**

Params | Data type | Required | Description
-------|-----------|----------|------------
url    | string    | YES| url of image
callback| function | YES| callback

### **3. Sample Request & Response**

#### **Request**

```js
MiniApi.getBase64FromUrl("https://example.png", (data) => {
        console.log(data);
 })
```

#### **Response**

Params | Data type | Description
-------|-----------|------------
data   | string    | Base64 string

### **4. Sample Request & Response**

No error and exception. If input wrong argument’s type will crash app

## **getImage(options, callback)​**

### **1. Function introduction**

Get image from gallery or capture new image

### **2. Function request & response params**

#### **Request**

Params | Data type | Required | Description
-------|-----------|----------|------------
options| object| YES| {type: "camera"/"album", editable: true/false}
callback| function| YES | callback

### **3. Sample Request & Response**

#### **Request**

```js
MiniApi.getImage({
                type: "camera",
                editable: true
            }, (data) => {
                console.log(data);
            })
```

#### **Response**

Params | Data type | Description
-------|-----------|------------
data   | string    | Base64 string

## **saveImage(base64Image, callback)​**

### **1. Function introduction**

Save image to gallery

### **2. Function request & response params**

#### **Request**

Params | Data type | Required | Description
-------|-----------|----------|------------
base64Image| string
callback| function

### **3. Sample Request & Response**

#### **Request**

```js
MiniApi.saveImage(data, (res) => {
                    console.log(res);
                })
```

#### **Response**

Params | Data type | Description
-------|-----------|------------
data   | boolean   | true if image is saved in gallery, false otherwise


## **getImageSize(uri, callback)​**

### **1. Function introduction**

get size image

### **2. Function request & response params**

#### **Request**

Params | Data type | Required | Description
-------|-----------|----------|------------
uri    |string     |YES
callback| function |YES| callback

### **3. Sample Request & Response**

N/A

