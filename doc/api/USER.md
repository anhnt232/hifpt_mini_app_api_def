# **USER​**

## **Old API**

### **callbackToApp(action)**

webview callback to app with action name

#### **Request**

Params | Data type | Required | Description
-------|-----------|----------|------------
action | object    | YES      | webview callback to app with action

#### **Action object**

Field | Data type| Required| Description
------|----------|---------|------------
action| string   | YES     | Action name needs to be processed by the main-app. posible values: "getToken", "getTokenWithDeviceId"

#### **Action descriptions**

Action name | Description | Simple
------------|-------------|-------
getToken| Get app's access token| {"action": "getToken"}
getTokenWithDeviceId || {"action":"getTokenWithDeviceId"}
tokenExpire | |{"action": "tokenExpire"}


### **initServices(data)**

App call webview with token data

#### **Request**

Field | Data type|Description
------|----------|-----------
data  | data object| data object sent to webview

### Data object

Field | Data type|Description| Simple
------|----------|-----------|---
token | string   | the app's access token, used for API authetification | "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJqdGkiOiI0OThlODBhNy04ZjcxLTQ1MGItODRhOC1hMjMzOTY5MTk5NjYiLCJpYXQiOjE2MzQ3ODYxMjYsImV4cCI6MTYzNDc4OTcyNn0.ZAIQDd03TXpF4gjacuajfzqXTRMeQEcWzYmpWYW3CRX7Ois5sM08yDrf8AktnDnLHOthxtlZSjpbsqeZguFw"
device_id_foxpay| string| Foxpay's device ID | "1234523532"
device_id| string| app's device ID | "1234523532"
merchant_id | string | | "001122"

#### Simple input

```json
{
    "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJqdGkiOiI0OThlODBhNy04ZjcxLTQ1MGItODRhOC1hMjMzOTY5MTk5NjYiLCJpYXQiOjE2MzQ3ODYxMjYsImV4cCI6MTYzNDc4OTcyNn0.ZAIQDd03TXpF4gjacuajfzqXTRMeQEcWzYmpWYW3CRX7Ois5sM08yDrf8AktnDnLHOthxtlZSjpbsqeZguFw",
    "device_id_foxpay": "1234523532",
    "device_id": "1234523532",
    "merchant_id": "001122"
}
```


## **New API**

Mini-app Authen Process

![authen](../mo_hinh_hifpt_v03-authen.drawio.png)

### **requestUserConsents(options, callback)​**

### **1. Function introduction**

Request to access user's information. It will show a bottom drawer to request for user information

To use this function, please follow the Request Permission document

### **2. Function request & response params**

#### **Request**

Params | Data type | Required | Description
-------|-----------|----------|------------
options| object    | YES      | An object with "permissions" field is an array for requested permission
callback| function | YES      | Callback function with data from the request

#### **Options object**

Field| Data type| Required| Description
-----|----------|---------|------------
permissions| array| YES   | An array contain object represent as a permission.

#### **Permission object**

Field| Data type| Required| Description| Value
-----|----------|---------|------------|------
role | string   | YES     | Permission role | "address",  "addressKyc", "city",  "countryCode", "countryName", "currentAddress", "deviceName", "deviceOs", "dobKyc", "email", "fullNameKyc", "gender", "genderKyc", "loyaltyPoints", "maritalStatus", "microShop", "name", "nationality", "nationalityKyc", "nickname", "phone", "preferedLanguage", "professional"
required| boolean| NO     | Set a permission role to be required sharing from user| true , false

#### **Response**

Params| Data type| Description
------|----------|------------
data  | object   | user's data
status| object   | status of requested permission

### **3. Sample Request & Response**

#### **Request**

```js
MiniApi.requestUserConsents({
    "permissions": [
        {
            "role": "name",
            "require": true
        },
        {
            "role": "phone"
        },
        {
            "role": "email",
        },
        {
            "role": "gender"
        },
        {
            "role": "dateOfBirth",
        },
        {
            "role": "identify"
        },
    ]
}, (data) => {
    console.log(data)
})
```

#### **Response**

```json
// Case 1: User accept sharing some data
{
    "data": {
        "name": "John Doe",
        "phone": "09xxxxxxxx"
    },
    "status": {
        "email": "denied",
        "name": "granted",
        "phone": "granted"
    }
}

// Case 2: User cancelled sharing data 
{
    "data" : {},
    "status" : "cancelled"
}

// Case 3: AppId doesn’t have permission to request sharing data / Wrong appId. Go to developers.momoapp.vn to request permission
{
    "data" : {},
    "status" : {}
}
```

### **4. Error code & handle exception**

Error Code| Description
----------|------------
-1        | no permission

## **getUserConsents(options, callback)​**

### **1. Function introduction**

Access user’s information that is already granted. Data will be returned through callback

To use this function, please follow the Request Permission document

### **2. Function request & response params**

#### **Request**

Params | Data type | Required | Description
-------|-----------|----------|------------
options| object| YES| An object with "permissions" key as an array for permission
callback| function| YES| Callback function with data from the request

#### **Options object**

Field| Data type| Required| Description
-----|----------|---------|------------
permissions| array| YES   | An array contain object represent as a permission.

#### **Permission object**

Field| Data type| Required| Description| Value
-----|----------|---------|------------|------
role | string   |YES      | Permission role| "address", "addressKyc", "city", "countryCode", "countryName", "currentAddress", "deviceName", "deviceOs", "dobKyc", "email", "fullNameKyc", "gender", "genderKyc", "loyaltyPoints", "maritalStatus", "microShop", "name", "nationality", "nationalityKyc", "nickname", "phone", "preferedLanguage", "professional"

#### **Response**

Field| Data type| Description
-----|----------|------------
data | object   | user's data
status| object  | status of requested permission

### **3. Sample Request & Response**

#### **Request**
```js
MiniApi.getUserConsents({
    "permissions": [
        {
            "role": "name",
        },
        {
            "role": "phone"
        },
        {
            "role": "email",
        },
    ]
}, (data) => {
    console.log(data)
})
```

#### **Response**
```json
{
    "data": {
        "name": "John Doe",
        "phone": "09xxxxxxxx"
    },
    "status": {
        "email": "denied",
        "name": "granted",
        "phone": "granted"
    }
}
```

### **4. Error code & handle exception**

Error Code| Description
----------|------------
-1        | no permission

## **getUserAuth(callback)​**

### **1. Function introduction**

Get User Authentication

### **2. Function request & response params**

#### **Request**

Params | Data type | Description
-------|-----------|------------
callback|function  | callback

#### **Response**

Params | Data type | Description
-------|-----------|------------
authCode| string| auth code
miniAppUserId| string | Id of Mini App user
partnerUserId| string | Id of partner user

### **3. Sample Request & Response**

#### **Request**

MiniApi.getUserAuth(callback)

#### **Response**
```json
{
    "authCode": "eyJ...",
    "miniAppUserId": "MRk...",
    "partnerUserId": "MRv..."
}
```
