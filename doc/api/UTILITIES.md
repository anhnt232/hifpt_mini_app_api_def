# **UTILITIES​**

## **copyToClipboard(text, toastMsg)​**

### **1. Function introduction**

Copy text to Clipboard and show toast

### **2. Function request & response params**

#### **Request**

Params | Data type | Required | Description
-------|-----------|----------|------------
text|string | YES| text to save to Clipboard
toastMsg| string| YES| text to show on toast

### **3. Sample Request & Response**

#### **Request**

    MiniAPI.copyToClipboard("Text to copyToClipboard","Text to show toast");

#### **Response**

N/A

## **share(data, callback)​**

### **1. Function introduction**

share text content to other apps

### **2. Function request & response params**

#### **Request**

Params | Data type | Required | Description
-------|-----------|----------|------------
data			
callback| callback| YES| callback

### **3. Sample Request & Response**

#### **Request**

    MiniAPI.share("Text to share",callback);

#### **Response**

N/A
