# **CAMERA​**

## **scanQRCode(data, callback)​**

### **1. Function introduction**

get data from qr code

### **2. Function request & response params**

#### **Request**

Params | Data type | Required | Description
-------|-----------|----------|------------
data   | deeplink	
callback| function| YES| callback

### **3. Sample Request & Response**

#### **Request**

```js
MiniApi.scanQRCode(data, (result) => {
    console.log(data);
})
```
#### **Response**

Params | Data type | Description
-------|-----------|------------
data   | object    | data in the deeplink
