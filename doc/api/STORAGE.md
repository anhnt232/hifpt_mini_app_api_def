# **STORAGE​**

## **getItem(key, callback)​**

### **1. Function introduction**

get data from Storage

### **2. Function request & response params**

#### **Request**

Params | Data type | Required | Description
-------|-----------|----------|------------
key    | string    |  YES     | key
callback| function |  YES     | return value

### **3. Sample Request & Response**

#### **Request**

```js
MiniApi.getItem('API', (data) => {
                console.log(data);
 })
 ```

#### **Response**

Params | Data type | Description
-------|-----------|------------
data   | string	   | value of the key

## **setItem(key, value)​**

### **1. Function introduction**

set data to Storage

### **2. Function request & response params**

Params | Data type | Required | Description
-------|-----------|----------|------------
key	   | string    |   YES    | key
value  | string    |   YES    | value

### **3. Sample Request & Response**

#### **Request**

```js
MiniApi.setItem('API', 'hello')
```
#### **Response**

void

## **removeItem(key)​**

### **1. Function introduction**

remove data from Storage

### **2. Function request & response params**

#### **Request**

Params | Data type | Required | Description
-------|-----------|----------|------------
key    | string    | YES      | key

### **3. Sample Request & Response**

#### **Request**

```js
MiniApi.removeItem('API')
```
#### **Response**

void

