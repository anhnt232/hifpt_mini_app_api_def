# **PAYMENT​**

## **Old API**

### **callPaymentWebkit(data)**

Webview call the app's payment service to pay the bill.

#### **Request**

Field | Data type|Description
------|----------|-----------
data  | data object| data object sent to app

### Data object

Field | Data type|Description |Posible values
------|----------|------------|-----
referral_code| string | |
trans_type| string | |
trans_id| string | |
service_type| string | |
total_amount| number | |
promo_code|  string | |
promo_value| number | |
items| array | array of pomotion object |

### Promotion object

Field | Data type|Description
------|----------|-----------
title | string |
amount| number |

### Simple input

```json
"data": {
  "referral_code": "0123211242",
  "trans_type": "FPTPLAY",
  "trans_id": "FPL000001",
  "service_type": "FPTPLAY",
  "total_amount": 20000,
  "promo_code": "abcd",
  "promo_value": -10000,
  "items": [{
    "title": "Hoa don dich vu FPLAY",
    "amount": 20000,
  }]
}
```


## **New API**

### **1. Function introduction**

Mini-app payment process

![image](../mo_hinh_hifpt_v03-payment.drawio.png)

Hi FPT app's Payment process

![hipayment](../hi-payment.jpg)


### **2. Function request & response params**

#### **Request**

Params | Data type | Required | Description
-------|-----------|----------|------------
phone  | text      |YES       |phone number

### **3. Sample Request & Response**

#### **Request**


### **4. Sample Request & Response**



