# **PHONE​**

openDialer(phone)​

### **1. Function introduction**

open Dialer to call

### **2. Function request & response params**

#### **Request**

Params | Data type | Required | Description
-------|-----------|----------|------------
phone  | text      |YES       |phone number

### **3. Sample Request & Response**

#### **Request**

    MiniApi.openDialer("0xxxxxxxx"); 

![image](mini-app_development-guideline_api_catalog-func.png)

### **4. Sample Request & Response**

Cannot catch error and exception. If input wrong argument’s type will crash app

