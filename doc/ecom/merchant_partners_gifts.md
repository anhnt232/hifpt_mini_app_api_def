## **merchant_partners_gifts**

## API: /hi-ecom-shopping-api/v1/web/merchant_partners/gifts

## **a. General information**

#### **Sequence Diagram**

![](./image/merchant_partners_gifts.jpg)

#### **call API(action)**

webview callback to app with API

#### **Request**

Params | Data type | Required | Description
-------|-----------|----------|------------
action | object    | YES      | webview callback to app with API
methods| POST      | YES      | webview callback to app with methods POST

#### **Action object**

Field | Data type| Required| Description
------|----------|---------|------------
action             | string   | YES     | Action needs to be processed by the main-app. posible values: "TOKEN", "User-Agent"
optional extra body| depends on the action type

#### **Action descriptions**

Action      | Description | Simple
------------|-------------|-------
getToken       | Get app's access token| {"TOKEN": "getToken"}
get User-Agent |                       | {"User-Agent":"get User-Agent"}


#### **Action with extra body if not forward to Shopping V2**

Type request | Description | Extra params |Type   | Structure
-------------|-------------|--------------|-------|----
get_all_gift_fr_product_id   |  Get all gift from product_id | data| JSON | { "type_request": "get_all_gift_fr_product_id", "data": {...}}
get_info_gift_fr_gift_id  |  Get information gift from gift_id | data| JSON | { "type_request": "get_info_gift_fr_gift_id", "data": {...}}




