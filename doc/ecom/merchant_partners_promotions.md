## **merchant_partners_promotions**

## API: /hi-ecom-shopping-api/v1/web/merchant_partners/promotions

## **a. General information**

#### **Sequence Diagram**
![](./image/merchant_partners_promotions.jpg)

#### **call API(action)**

webview callback to app with API

#### **Request**

Params | Data type | Required | Description
-------|-----------|----------|------------
action | object    | YES      | webview callback to app with API
methods| POST      | YES      | webview callback to app with methods POST

#### **Action object**

Field | Data type| Required| Description
------|----------|---------|------------
action             | string   | YES     | Action needs to be processed by the main-app. posible values: "TOKEN", "User-Agent"
optional extra body| depends on the action type

#### **Action descriptions**

Action      | Description | Simple
------------|-------------|-------
getToken       | Get app's access token| {"TOKEN": "getToken"}
get User-Agent |                       | {"User-Agent":"get User-Agent"}


#### **Action with extra body**

Type request | Description | Extra params |Type   | Structure
-------------|-------------|--------------|-------|----
get_promotion_by_id   |  Get promotion by id| data| JSON | { "type_request": "get_promotion_by_id", "data": {...}}
get_promotion_list  |  Get promotion list| data| JSON | { "type_request": "get_promotion_list", "data": {...}}
active_voucher_fr_order_id_payment  |  Active voucher fr order id payment| data| JSON | { "type_request": "active_voucher_fr_order_id_payment", "data": {...}}
get_warranty_location  |  Get warranty location| data| JSON | { "type_request": "get_warranty_location", "data": {...}}

## **b. Action object optional extra body**

### **- get_promotion_by_id**

### **action object**

Field | Data type| Required| Description
------|----------|---------|------------
type_request     | string   | YES     | Only input "get_promotion_by_id"
data             | json     | YES     | Conditions use get info
promo_id         | int      | YES     | Request is type integer and it is Id of corresponding promotion
merchant_id      | string   | YES     | merchant_id is valid in [Merchant Partner](./Merchant_Partner.md)

[Merchant Partner](./Merchant_Partner.md)

### Sequence Diagram

![](./image/get_promotion_by_id.jpg)

### Sample Request & Response

```json
{
    "type_request": "get_promotion_by_id",
    "data": {
        "promo_id": 20, 
        "merchant_id": "VUANEM"
    }
}
```

### Response

```json
{
    "status": 1, 
    "msg": "Lấy thông tin thành công!", 
    "detail": {
        "promo_id": 20, 
        "title": "Mua nệm đẹp - Freeship linh đình - Tặng quà đỉnh", 
        "image": "https://hi-static.fpt.vn/sys/hifpt/pnc_pdx/ftg-shopping/promotion/tuan_1_thang_6_khuyen_mai_2.png", 
        "description": "Hi FPT xin ra mắt chương trình \"Tuần lễ ra mắt Elmich\" - đồ gia dụng chất lượng tiêu chuẩn châu Âu Elmich với đơn hàng 1 món cũng Freeship. <br>\r\n<br>\r\n- Đơn hàng Elmich từ 1.5TR tặng Bình giữ nhiệt inox 304 Elmich EL-8012OL dung tích 600ml trị giá 359.000VND <br>\r\n- Đơn hàng Elmich từ 3TR tặng Máy sấy tóc Elmich HDE-1810OL trị giá 850.000VND <br>\r\n- Đơn hàng Elmich từ 5TR tặng Máy xay sinh tố Elmich BLE-1840OL trị giá 1.459.000VND <br>\r\n<br>\r\n◾ Thời gian diễn ra chương trình: 26/5 - 05/06 <br>\r\n◾ Điều kiện: Đặt đơn hàng Elmich trên ứng dụng Hi FPT <br>\r\n◾ Khu vực Freeship: Toàn quốc <br>\r\n<br>\r\nSố lượng quà tặng cực kì có hạn, trong trường hợp hết quà tặng theo chương trình thì sẽ được thay thế bằng quà tặng có giá trị gần nhất.",
        "t_publish": "23/06/2022", 
        "merchant_id": "VUANEM", 
        "other_promotion": []
    }
}
```

### Application

![](./image/get_promotion_by_id_screen.jpg)

#### **- get_promotion_list**

### **action object**

Field | Data type| Required| Description
------|----------|---------|------------
type_request     | string   | YES     | Only input "get_promotion_list"
data             | json     | YES     | Conditions use get info
merchant_id      | string   | YES     | merchant_id is valid in [Merchant Partner](./Merchant_Partner.md)

### Sequence Diagram

![](./image/get_promotion_list.jpg)

### Sample Request & Response

```json
{
    "type_request": "get_promotion_list",
    "data": { 
        "merchant_id": "VUANEM"
    }
}
```

### Response

```json
{
    "status": 1, 
    "msg": "Lấy thông tin thành công!", 
    "detail": [{
        "brief": "Hi FPT tặng bạn mã Voucher giảm giá 650K cho đơn hàng Laptop.",
        "image": "https://hi-static.fpt.vn/sys/hifpt/pnc_pdx/ftg-shopping/promotion/tuan_1_thang_6_khuyen_mai_2.png",
        "promo_id": 20,
        "t_publish": "23/06/2022",
        "title": "Mua nệm đẹp - Freeship linh đình - Tặng quà đỉnh"
    }]
}
```

### Application

![](./image/get_promotion_list_webkit_screen.jpg)

### **- active_voucher_fr_order_id_payment**

### **action object**

Field | Data type| Required| Description
------|----------|---------|------------
type_request     | string   | YES     | Only input "active_voucher_fr_order_id_payment"
data             | json     | YES     | Conditions use get info

### Sequence Diagram

![](./image/active_voucher_fr_order_id_payment.jpg)

### Sample Request & Response

```json

```

### Response

```json

```

### Application

![](.jpg)

### **- get_warranty_location**

### **action object**

Field | Data type| Required| Description
------|----------|---------|------------
type_request     | string   | YES     | Only input "get_warranty_location"
data             | json     | YES     | Conditions use get info

### Sequence Diagram

![](./image/get_warranty_location.jpg)

### Sample Request & Response

```json

```

### Response

```json

```

### Application

![]()