## **merchant_partners_products**

## API: /hi-ecom-shopping-api/v1/web/merchant_partners/products

## **a. General information**

#### **Sequence Diagram**

![](./image/merchant_partners_products.jpg)

#### **call API(action)**

webview callback to app with API

#### **Request**

Params | Data type | Required | Description
-------|-----------|----------|------------
action | object    | YES      | webview callback to app with API
methods| POST      | YES      | webview callback to app with methods POST

#### **Action object**

Field | Data type| Required| Description
------|----------|---------|------------
action             | string   | YES     | Action needs to be processed by the main-app. posible values: "TOKEN", "User-Agent"
optional extra body| depends on the action type

#### **Action descriptions**

Action      | Description | Simple
------------|-------------|-------
getToken       | Get app's access token| {"TOKEN": "getToken"}
get User-Agent |                       | {"User-Agent":"get User-Agent"}


#### **Action with extra body**

Type request | Description | Extra params |Type   | Simple
-------------|-------------|--------------|-------|----
get_homepage_products   |  Call get_cart information| data| JSON | { "type_request": "get_homepage_products", "data": {...}}
get_category  |  Add product to cart | data| JSON | { "type_request": "get_category", "data": {...}}
get_product_fr_category_id  |  Edit/Update cart (ignore merchant)| data| JSON | { "type_request": "get_product_fr_category_id", "data": {...}}
get_product_fr_product_sku  |  Call cal prices (dùng cho vé sự kiện) | data| JSON | { "type_request": "get_product_fr_product_sku", "data": {...}}
get_variants  |  Call cal prices (dùng cho vé sự kiện) | data| JSON | { "type_request": "get_variants", "data": {...}}
search_products_by_keyword  |  Call cal prices (dùng cho vé sự kiện) | data| JSON | { "type_request": "search_products_by_keyword", "data": {...}}
search_products_by_condition  |  Call cal prices (dùng cho vé sự kiện) | data| JSON | { "type_request": "search_products_by_condition", "data": {...}}

## **b. Action object optional extra body**

### **- get_homepage_products**

### **action object**

Field | Data type| Required| Description
------|----------|---------|------------
type_request     | string   | YES     | Only input "cancel_order"
data             | json     | YES     | Conditions use get info
merchant_id      | json     | YES     | merchant_id is valid in [Merchant Partner](./Merchant_Partner.md)

### Sequence Diagram

![](./image/get_homepage_products.jpg)

### Sample Request & Response

```json
{
    "data": {
        "merchant_id": "VUANEM"
    }, 
    "type_request": "get_homepage_products"
}
```

### Response

```json
{
    "detail": {
        "groups_products": [{
                "group_code": "PACKAGE", 
                "group_name": "Gói Wifi", 
                "navigate_category_id": 54, 
                "active": 1, 
                "priority": 1
            }, {
                "group_code": "POPULARITY", 
                "group_name": "Sản phẩm phổ biến", 
                "navigate_category_id": 40, 
                "active": 1,
                "priority": 1
            }, {
                "group_code": "ON_DISCOUNT", 
                "group_name": "Đang khuyến mãi", 
                "navigate_category_id": 39, 
                "active": 1,
                "priority": 2
            }
        ]
    },
    "msg": "Thành công",
    "status": 1
}
```

### Application

![](./image/get_homepage_products_screen.jpg)

### **- get_category**

### **action object**

Field | Data type| Required| Description
------|----------|---------|------------
type_request     | string   | YES     | Only input "get_category"
data             | json     | YES     | Conditions use get info
merchant_id      | json     | YES     | merchant_id is valid in [Merchant Partner](./Merchant_Partner.md)

### Sequence Diagram

![](./image/get_category.jpg)

### Sample Request & Response

```json
{
    "data": {
        "merchant_id": "VUANEM"
    }, 
    "type_request": "get_category"
}
```

### Response

```json
{
    "status": 1, 
    "msg": "Thành công", 
    "detail": {
        "list_category": [{
                "category_id": 39, 
                "merchant_id": "VUANEM", 
                "category_name": "Đệm bông ép", 
                "category_src_img": "https://hi-static.fpt.vn/sys/hifpt/pnc_pdx/ftg-shopping/products/vuanem/vector.png", 
                "category_priority": 1, 
                "category_state": 1
            }, {
                "category_id": 40, 
                "merchant_id": "VUANEM", 
                "category_name": "Đệm cao su", 
                "category_src_img": "https://hi-static.fpt.vn/sys/hifpt/pnc_pdx/ftg-shopping/products/vuanem/1101002002010_1.jpg", 
                "category_priority": 1, 
                "category_state": 1
            }, {
                "category_id": 41, 
                "merchant_id": "VUANEM", 
                "category_name": "Đệm lò xo", 
                "category_src_img": "https://hi-static.fpt.vn/sys/hifpt/pnc_pdx/ftg-shopping/products/vuanem/1102001008002_1.jpg", 
                "category_priority": 1, 
                "category_state": 1
            }, {
                "category_id": 42, 
                "merchant_id": "VUANEM", 
                "category_name": "Đệm khác", 
                "category_src_img": "https://hi-static.fpt.vn/sys/hifpt/pnc_pdx/ftg-shopping/products/vuanem/1104005002001_1.jpg", 
                "category_priority": 1, 
                "category_state": 1
            }, {
                "category_id": 43, 
                "merchant_id": "VUANEM", 
                "category_name": "Ruột chăn", 
                "category_src_img": "https://hi-static.fpt.vn/sys/hifpt/pnc_pdx/ftg-shopping/products/vuanem/1302001007001_1.jpg", 
                "category_priority": 1, 
                "category_state": 1
            }, {
                "category_id": 44, 
                "merchant_id": "VUANEM", 
                "category_name": "Ruột gối", 
                "category_src_img": "https://hi-static.fpt.vn/sys/hifpt/pnc_pdx/ftg-shopping/products/vuanem/1301022005001_1.jpg", 
                "category_priority": 1, 
                "category_state": 1
            }, {
                "category_id": 45, 
                "merchant_id": "VUANEM", 
                "category_name": "Vỏ chăn", 
                "category_src_img": "https://hi-static.fpt.vn/sys/hifpt/pnc_pdx/ftg-shopping/products/vuanem/1206001009003_1.jpg", 
                "category_priority": 1, 
                "category_state": 1
            }, {
                "category_id": 46, 
                "merchant_id": "VUANEM", "category_name": "Tấm bảo vệ đệm",
                "category_src_img": "https://hi-static.fpt.vn/sys/hifpt/pnc_pdx/ftg-shopping/products/vuanem/1304006001002_1.jpg", 
                "category_priority": 1, 
                "category_state": 1
            }
        ]
    }
}
```

### Application

![](./image/get_category_screen.jpg)

### **- get_product_fr_category_id**

### **action object**

Field | Data type| Required| Description
------|----------|---------|------------
type_request     | string   | YES     | Only input "get_product_fr_category_id"
data             | json     | YES     | Conditions use get info
merchant_id      | json     | YES     | merchant_id is valid in [Merchant Partner](./Merchant_Partner.md)

### Sequence Diagram

![](./image/get_product_fr_category_id.jpg)

### **- get_product_fr_product_sku**

### **action object**

Field | Data type| Required| Description
------|----------|---------|------------
type_request     | string     | YES     | Only input "get_product_fr_product_sku"
data             | json       | YES     | Conditions use get info
sku              | string     | YES     | Like product code, used to identify each product separately
merchant_id      | string     | YES     | merchant_id is valid in [Merchant Partner](./Merchant_Partner.md)

### Sequence Diagram

![](./image/get_product_fr_product_sku.jpg)

### Sample Request & Response

```json
{
    "data": {
        "sku": "1103005007001",
        "merchant_id": "VUANEM"
    }, 
    "type_request": "get_product_fr_product_sku"
}
```

### Response

```json
{
    "status": 1, 
    "msg": "Thành công", 
    "detail": {
        "main_products": {
            "product_id": 236, 
            "merchant_id": "VUANEM", 
            "agent_id": "VUANEM", 
            "sku": "1103005007001", 
            "product_src_img": "https://hi-static.fpt.vn/sys/hifpt/pnc_pdx/ftg-shopping/products/vuanem/1103005007001_1.jpg", 
            "product_supplier_name": "Gummi", 
            "product_name": "N\u1ec7m cao su Gummi Standard", 
            "product_option": "100x200", 
            "icon_option": null, 
            "secification": null, 
            "product_desciption": "<h3>\nNệm cao su Gummi Standard\n</h3>\n<p>\n<strong>Cấu tạo </strong>\n</p>\n<p>\nNệm cao su Gummi Standard có lõi sử dụng 100% cao su thiên nhiên không lẫn\ntạp chất và các thành phần chứa tạp chất. Một bên bề mặt là lỗ tròn, một\nbên bề mặt là lỗ vuông nên nệm luôn có độ thông thoáng không gây hầm bí,\ncùng với đó là độ đàn hồi tối ưu phù hợp cho mọi đối tượng.\n</p>\n<p>\nSản phẩm nệm cao su Gummi Standard còn sử dụng công nghệ xử lý mùi tối ưu,\nkhông có hiện tượng mùi cao su mang đến sự an toàn tuyệt đối. Bên cạnh đó,\nsản phẩm không gây kích ứng da, không có mùi khó chịu ảnh hưởng đến sức\nkhoẻ người sử dụng. Đặc biệt, nệm cao su Gummi Standard còn sở hữu mùi\nvanila dịu nhẹ giúp người dùng ngủ êm ái hơn.\n</p>\n<p>\nÁo nệm được làm bằng chất liệu thun cao cấp, thoáng mát tự nhiên. Người sử\ndụng hoàn toàn có thể dễ dàng tháo rời và vệ sinh dễ dàng.\n</p>\n<p>\n<strong>Sản phẩm đạt tiêu chuẩn chất lượng quốc tế </strong>\n</p>\n<p>\nNệm cao su Gummi Standard đạt chứng nhận của Quatest 3 - Sản phẩm không\nchứa hoá chất độc hại và tiêu chuẩn về hoá lý nên đảm bảo tuyệt đối an toàn\nvề sức khoẻ. Người dùng hoàn toàn có thể yên tâm sử dụng và làm quà tặng\ncho người thân, bạn bè.\n</p>\n<p>\n<strong>Lợi ích khi sử dụng nệm cao su Gummi Standard</strong>\n</p>\n<p>\nNệm cao su Gummi Standard có độ đàn hồi tối ưu cùng khả năng nâng đỡ đều\ncác phần trên cơ thể giúp cho phần cột sống của người nằm không bị võng\nxuống. không gây đau lưng. đau nhức xương khớp hay mệt mỏi mỗi khi thức\ndậy.\n</p>\n<p>\nNgoài ra, nệm còn có khả năng thoáng khí cao nhờ cấu trúc hàng nghĩ lỗ lớn\nnhỏ ở cả 2 bề mặt nên có khả năng thoát ẩm cao và nhanh khô sau mỗi lần vệ\nsinh.\n</p>\n<p>\nĐặc biệt đối với dòng nệm cao su Gummi Standard, người dùng có thể mua nệm\ntheo kích thước tiêu chuẩn hoặc đặt nệm theo nhu cầu cá nhân. Đối với khu\nvực miền Bắc, thời gian nhận sản phẩm từ 25-50 ngày kể từ ngày đặt. Còn với\ncác tỉnh phía Nam, người dùng có thể nhận hàng sau 10-15 ngày đặt sản phẩm.\n</p>",
            "usual_price": 7100000, 
            "discount_price": 4686000, 
            "unit_type": null, 
            "product_active": 1, 
            "available_area": null, 
            "description": null, 
            "description_img": null, 
            "slide_img": ["https://hi-static.fpt.vn/sys/hifpt/pnc_pdx/ftg-shopping/products/vuanem/1103005007001_1.jpg", "https://hi-static.fpt.vn/sys/hifpt/pnc_pdx/ftg-shopping/products/vuanem/1103005007001_1.jpg", "https://hi-static.fpt.vn/sys/hifpt/pnc_pdx/ftg-shopping/products/vuanem/1103005007001_2.jpg", "https://hi-static.fpt.vn/sys/hifpt/pnc_pdx/ftg-shopping/products/vuanem/1103005007001_3.jpg", "https://hi-static.fpt.vn/sys/hifpt/pnc_pdx/ftg-shopping/products/vuanem/1103005007001_4.jpg", "https://hi-static.fpt.vn/sys/hifpt/pnc_pdx/ftg-shopping/products/vuanem/1103005007001_5.jpg", "https://hi-static.fpt.vn/sys/hifpt/pnc_pdx/ftg-shopping/products/vuanem/1103005007001_6.jpg", "https://hi-static.fpt.vn/sys/hifpt/pnc_pdx/ftg-shopping/products/vuanem/1103005007001_7.jpg", "https://hi-static.fpt.vn/sys/hifpt/pnc_pdx/ftg-shopping/products/vuanem/1103005007001_8.jpg", "https://hi-static.fpt.vn/sys/hifpt/pnc_pdx/ftg-shopping/products/vuanem/1103005007001_9.jpg"], 
            "service_type": "PRODUCT", 
            "new": 0, 
            "quantity_current": 349, 
            "variants_tags": ["100x200"], 
            "info_agent": {
                "agent_id": "VUANEM", 
                "merchant_id": "VUANEM", 
                "service_type": "VN", 
                "agent_name": "Vua nệm", 
                "src_img": "https://hi-static.fpt.vn/sys/hifpt/pnc_pdx/ecom/agent/icon_Synnex_FPT.png", 
                "list_payment_method": "ONLINE", 
                "payment_methods": "INTERNATIONAL;DOMESTIC;WALLET_FOXPAY", 
                "description_error": "Hiện tại dịch vụ chỉ hỗ trợ online", 
                "invalid_locations": null
            }, 
            "could_option": 0, 
            "main_product_quantity": 50
        }, 
        "variants": {
            "list_variants": [{
                "text": "Kích thước", 
                "variant_type": "size", 
                "variants": ["100x200", "120x200", "140x200", "160x200", "180x200", "200x200", "200x220"]
            }, {
                "text": "Trọng lượng", 
                "variant_type": "color", 
                "variants": []
            }, {
                "text": "Độ dày", 
                "variant_type": "thick", 
                "variants": []
            }], 
            "variant_product": {
                "product_id": 236, 
                "merchant_id": "VUANEM", 
                "agent_id": "VUANEM", 
                "sku": "1103005007001", 
                "product_src_img": "https://hi-static.fpt.vn/sys/hifpt/pnc_pdx/ftg-shopping/products/vuanem/1103005007001_1.jpg", 
                "product_supplier_name": "Gummi", 
                "product_name": "Nệm cao su Gummi Standard", 
                "product_option": "100x200", 
                "icon_option": null, 
                "secification": null, 
                "product_desciption": "...", 
                "usual_price": 7100000, 
                "discount_price": 4686000, 
                "unit_type": null, 
                "product_active": 1, 
                "available_area": null, 
                "description": null, 
                "description_img": null, 
                "slide_img": ["https://hi-static.fpt.vn/sys/hifpt/pnc_pdx/ftg-shopping/products/vuanem/1103005007001_1.jpg", "https://hi-static.fpt.vn/sys/hifpt/pnc_pdx/ftg-shopping/products/vuanem/1103005007001_1.jpg", "https://hi-static.fpt.vn/sys/hifpt/pnc_pdx/ftg-shopping/products/vuanem/1103005007001_2.jpg", "https://hi-static.fpt.vn/sys/hifpt/pnc_pdx/ftg-shopping/products/vuanem/1103005007001_3.jpg", "https://hi-static.fpt.vn/sys/hifpt/pnc_pdx/ftg-shopping/products/vuanem/1103005007001_4.jpg", "https://hi-static.fpt.vn/sys/hifpt/pnc_pdx/ftg-shopping/products/vuanem/1103005007001_5.jpg", "https://hi-static.fpt.vn/sys/hifpt/pnc_pdx/ftg-shopping/products/vuanem/1103005007001_6.jpg", "https://hi-static.fpt.vn/sys/hifpt/pnc_pdx/ftg-shopping/products/vuanem/1103005007001_7.jpg", "https://hi-static.fpt.vn/sys/hifpt/pnc_pdx/ftg-shopping/products/vuanem/1103005007001_8.jpg", "https://hi-static.fpt.vn/sys/hifpt/pnc_pdx/ftg-shopping/products/vuanem/1103005007001_9.jpg"],
                "service_type": "PRODUCT", 
                "new": 0, 
                "quantity_current": 50, 
                "variants_tags": ["100x200"], 
                "info_agent": {
                    "agent_id": "VUANEM", 
                    "merchant_id": "VUANEM", 
                    "service_type": "VN", 
                    "agent_name": "Vua nệm", 
                    "src_img": "https://hi-static.fpt.vn/sys/hifpt/pnc_pdx/ecom/agent/icon_Synnex_FPT.png", 
                    "list_payment_method": "ONLINE", 
                    "payment_methods": "INTERNATIONAL;DOMESTIC;WALLET_FOXPAY", 
                    "description_error": "Hiện tại dịch vụ chỉ hỗ trợ online", 
                    "invalid_locations": null
                }, 
                "could_option": 0
            }
        }
    }
}
```

### Application

![](./image/get_product_fr_product_sku_screen.jpg)

### **- get_variants**

### Sequence Diagram

![](./image/get_variants.jpg)

### **- search_products_by_keyword**

### Sequence Diagram

![](./image/search_products_by_keyword.jpg)

### **- search_products_by_condition**

### **action object**

Field | Data type| Required| Description
------|----------|---------|------------
type_request     | string   | YES     | Only input "search_products_by_condition"
data             | json     | YES     | Conditions use get info
beginPrice       | integer  | NO      | Can set default null
endPrice         | integer  | NO      | Can set default null
category_id      | integer  | NO      | Can set default null
keyword          | string   | NO      | Can set default ""
limit            | integer  | YES     | Quantity of products taken out
merchant_id      | string   | YES     | merchant_id is valid in [Merchant Partner](./Merchant_Partner.md)
page             | integer  | YES     | Used for pagination
sortNameBy       | string   | NO      | Can set default null
sortPriceBy      | string   | NO      | Can set default null

### Sequence Diagram

![](./image/search_products_by_condition.jpg)

### Sample Request & Response

```json
{
    "data" : {
        "beginPrice": 0,
        "endPrice": 1000000,
        "category_id": null,
        "keyword": "",
        "limit": 27,
        "merchant_id": "VUANEM",
        "page": 1,
        "sortNameBy": "ASC",
        "sortPriceBy": "DESC"
    },
    "type_request": "search_products_by_condition"
}
```

### Response

List of all products corresponding to the condition

### Application

![](./image/search_products_by_condition_screen.jpg)