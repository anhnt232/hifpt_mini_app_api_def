## **merchant_partners_bills**

## API: /hi-ecom-shopping-api/v1/web/merchant_partners/bills

## **a. General information**

#### **Sequence Diagram**
![](./image/merchant_partners_bills.jpg)

#### **call API(action)**

webview callback to app with API

#### **Request**

Params | Data type | Required | Description
-------|-----------|----------|------------
action | object    | YES      | webview callback to app with API
methods| POST      | YES      | webview callback to app with methods POST

#### **Action object**

Field | Data type| Required| Description
------|----------|---------|------------
action             | string   | YES     | Action needs to be processed by the main-app. posible values: "TOKEN", "User-Agent"
optional extra body| depends on the action type

#### **Action descriptions**

Action      | Description | Simple
------------|-------------|-------
getToken       | Get app's access token| {"TOKEN": "getToken"}
get User-Agent |                       | {"User-Agent":"get User-Agent"}


#### **Action with extra body**

Type request | Description | Extra params |Type   | Structure
-------------|-------------|--------------|-------|----
get_detail   |  Call merchant partners get detail from order id payment| data| JSON | { "type_request": "get_detail", "data": {...}}
get_my_bill  |  Call merchant partners get all bill information purchased by customer| data| JSON | { "type_request": "get_my_bill", "data": {...}}

## **b. Action object optional extra body**

### **- get_detail**

### **action object**

Field | Data type| Required| Description
------|----------|---------|------------
type_request     | string   | YES     | Type option when use same API
data             | json     | YES     | Conditions use get info
order_id_payment | string   | YES     | Order id payment of bill
merchant_id      | string   | YES     | merchant_id is valid in [Merchant Partner](./Merchant_Partner.md)

### Sequence Diagram

![](./image/get_detail.jpg)

### Sample Request & Response

```json
{
    "type_request": "get_detail",
    "data": {
        "order_id_payment": "VN00006621", 
        "merchant_id": "VUANEM"
    }
}
```

### Response

```json
{
    "status": 1, 
    "msg": "Th\u00e0nh c\u00f4ng", 
    "detail": {
        "order_id": "VN00006621", 
        "trans_id": 6621, 
        "payment_status": "Ch\u1edd thanh to\u00e1n", 
        "order_id_payment": "VN00006621", 
        "t_create": "03/06/2022 15:31:05", 
        "payment_method": "COD", 
        "amount": 6021000, 
        "amount_voucher": "", 
        "promotion_code": "", 
        "note": "", 
        "total_amount_finish": 6021000, 
        "fullname": "thunghiem", 
        "phone_nb": "0965656565", 
        "email": "van@gmail.com", 
        "full_address": "jdjdj \u1ebbuu, Ph\u01b0\u1eddng 15, Q8, H\u1ed3 Ch\u00ed Minh", 
        "type_address": "HOUSE", 
        "address": "jdjdj \u1ebbuu", 
        "list_item": [{
            "id": 11781, 
            "trans_id": 6621, 
            "product_id": 320, 
            "quantity": 1, 
            "unit_price": 6021000, 
            "discount": "", 
            "sku": "1102002014011", 
            "product_src_img": "https://hi-static.fpt.vn/sys/hifpt/pnc_pdx/ftg-shopping/products/vuanem/1102002014011_1.jpg", 
            "product_name": "N\u1ec7m l\u00f2 xo Dunlopillo Venus Supreme", 
            "product_option": "180x200"
        }], 
        "list_gift": [], 
        "list_tracking": [{
            "status": 1, 
            "str_time": "03/06/2022 15:31:05", 
            "text": "\u0110\u1eb7t h\u00e0ng th\u00e0nh c\u00f4ng", 
            "icon_img": "https://hi-static.fpt.vn/sys/hifpt/pnc_pdx/ecom/confirm.png", 
            "color": "#69DBA4"
        }], 
        "dict_order_state": {
            "order_state": "CREATE_SUCCESS", 
            "description_state": "\u0110\u1eb7t h\u00e0ng th\u00e0nh c\u00f4ng", 
            "rule_next_step": null, 
            "color_code": "#69DBA4", 
            "status_ship60": null}, 
            "estimate_time": "Trong v\u00f2ng 3-5 ng\u00e0y l\u00e0m vi\u1ec7c t\u1eeb th\u1eddi \u0111i\u1ec3m \u0111\u01a1n h\u00e0ng \u0111\u01b0\u1ee3c x\u00e1c nh\u1eadn (tr\u1eeb Ch\u1ee7 nh\u1eadt, L\u1ec5 T\u1ebft).", 
            "permission": "CANCEL", 
            "info_bill": {
                "id": 192, 
                "trans_id": 6621, 
                "company_name": "", 
                "tax_code": "", 
                "company_address": "", 
                "email": ""
            }, 
            "is_exported_bill": 1, 
            "appointment_date": "", 
            "timezone": "", 
            "pilot_state": [{
                "state": "CHO_THANH_TOAN", 
                "text": "Ch\u1edd thanh to\u00e1n"
            }, {
                "state": "DANG_XU_LY", 
                "text": "
                \u0110ang x\u01b0\u0309 ly\u0301"
            }, {
                "state": "DA_HOAN_TAT", 
                "text": "\u0110a\u0303 hoa\u0300n t\u00e2\u0301t"
            }, {
                "state": "DA_HUY", 
                "text": "\u0110a\u0303 hu\u0309y"
            }], 
            "state": "DANG_XU_LY"
        }
    }
```

### Application

![](./image/get_detail_webkit_screen.jpg)

### **- get_my_bill**

### **action object**

Field | Data type| Required| Description
------|----------|---------|------------
type_request     | string   | YES     | Type option when use same API
data             | json     | YES     | Conditions use get info
merchant_id      | string   | YES     | merchant_id is valid in [Merchant Partner](./Merchant_Partner.md)

### Sequence Diagram

![](./image/get_my_bill.jpg)

### Sample Request & Response

```json
{
    "data": {
        "merchant_id": "VUANEM"
    }, 
    "type_request": "get_my_bill"
}
```

### Response

```json
{
    "status": 1, 
    "msg": "Th\u00e0nh c\u00f4ng", 
    "detail": {
        "list_my_bill": [{
            "order_id": "VN00006621", 
            "trans_id": 6621, 
            "payment_status": "Ch\u1edd thanh to\u00e1n", "order_id_payment": "VN00006621", 
            "t_create": "03/06/2022 15:31:05", 
            "payment_method": "COD", 
            "amount": 6021000, 
            "amount_voucher": "", 
            "promotion_code": "", 
            "note": "", 
            "total_amount_finish": 6021000, 
            "fullname": "thunghiem", 
            "phone_nb": "0965656565", 
            "email": "van@gmail.com", 
            "full_address": "jdjdj \u1ebbuu, Ph\u01b0\u1eddng 15, Q8, H\u1ed3 Ch\u00ed Minh", 
            "type_address": "HOUSE", 
            "address": "jdjdj \u1ebbuu", 
            "list_item": [{
                "id": 11781, 
                "trans_id": 6621, 
                "product_id": 320, 
                "quantity": 1, 
                "unit_price": 6021000, 
                "discount": "", 
                "sku": "1102002014011", 
                "product_src_img": "https://hi-static.fpt.vn/sys/hifpt/pnc_pdx/ftg-shopping/products/vuanem/1102002014011_1.jpg", 
                "product_name": "N\u1ec7m l\u00f2 xo Dunlopillo Venus Supreme", 
                "product_option": 
                "180x200"
            }], 
            "list_gift": [], 
            "list_tracking": [{
                "status": 1, 
                "str_time": "03/06/2022 15:31:05", 
                "text": "\u0110\u1eb7t h\u00e0ng th\u00e0nh c\u00f4ng", 
                "icon_img": "https://hi-static.fpt.vn/sys/hifpt/pnc_pdx/ecom/confirm.png", 
                "color": "#69DBA4"
            }], 
            "dict_order_state": {
                "order_state": "CREATE_SUCCESS", 
                "description_state": "\u0110\u1eb7t h\u00e0ng th\u00e0nh c\u00f4ng", 
                "rule_next_step": null, 
                "color_code": "#69DBA4", 
                "status_ship60": null
            }, 
            "estimate_time": "Trong v\u00f2ng 3-5 ng\u00e0y l\u00e0m vi\u1ec7c t\u1eeb th\u1eddi \u0111i\u1ec3m \u0111\u01a1n h\u00e0ng \u0111\u01b0\u1ee3c x\u00e1c nh\u1eadn (tr\u1eeb Ch\u1ee7 nh\u1eadt, L\u1ec5 T\u1ebft).", 
            "permission": "CANCEL", 
            "info_bill": {
                "id": 192, 
                "trans_id": 6621, 
                "company_name": "", 
                "tax_code": "", 
                "company_address": "", 
                "email": ""
            }, 
            "is_exported_bill": 1, 
            "appointment_date": "", 
            "timezone": "", 
            "pilot_state": [{
                "state": "CHO_THANH_TOAN", 
                "text": "Ch\u1edd thanh to\u00e1n"
            }, {
                "state": "DANG_XU_LY", 
                "text": "\u0110ang x\u01b0\u0309 ly\u0301"
            }, {
                "state": "DA_HOAN_TAT", 
                "text": "\u0110a\u0303 hoa\u0300n t\u00e2\u0301t"
            }, {
                "state": "DA_HUY", 
                "text": "\u0110a\u0303 hu\u0309y"
            }], 
            "state": "DANG_XU_LY"
        }]
        [{...}]
        .
        .
        .
    }
}
```

### Application

![](./image/get_my_bill_webkit_screen.jpg)