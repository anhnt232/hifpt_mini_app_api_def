## **merchant_partners_carts**

## API: /hi-ecom-shopping-api/v1/web/merchant_partners/carts

## **a. General information**

#### **Sequence Diagram**

![](./image/merchant_partners_carts.jpg)

#### **call API(action)**

webview callback to app with API

#### **Request**

Params | Data type | Required | Description
-------|-----------|----------|------------
action | object    | YES      | webview callback to app with API
methods| POST      | YES      | webview callback to app with methods POST

#### **Action object**

Field | Data type| Required| Description
------|----------|---------|------------
action             | string   | YES     | Action needs to be processed by the main-app. posible values: "TOKEN", "User-Agent"
optional extra body| depends on the action type

#### **Action descriptions**

Action      | Description | Simple
------------|-------------|-------
getToken       | Get app's access token| {"TOKEN": "getToken"}
get User-Agent |                       | {"User-Agent":"get User-Agent"}


#### **Action with extra body**

Type request | Description | Extra params |Type   | Simple
-------------|-------------|--------------|-------|----
get_cart   |  Call get cart information| data| JSON | { "type_request": "get_cart", "data": {...}}
add_cart  |  Add product to cart | data| JSON | { "type_request": "add_cart", "data": {...}}
edit_cart  |  Edit/Update cart (ignore merchant)| data| JSON | { "type_request": "edit_cart", "data": {...}}
cal_prices  |  Call cal prices (for event tickets) | data| JSON | { "type_request": "cal_prices", "data": {...}}


## **b. Action object optional extra body**

### **- get_cart**

### **action object**

Field | Data type| Required| Description
------|----------|---------|------------
type_request     | string   | YES     | Only input "get_cart"
data             | json     | YES     | Conditions use get info
merchant_id      | json     | YES     | merchant_id is valid in [Merchant Partner](./Merchant_Partner.md)

### Sequence Diagram

![](./image/get_cart.jpg)

### Sample Request & Response

```json
{
    "data": {
        "merchant_id": "VUANEM"
    },
    "type_request": "get_cart"
}
```

### Response

```json
{
    "status": 1, 
    "msg": "Thành công", 
    "detail": {
        "list_buying": [{
            "product_id": 226, 
            "merchant_id": "VUANEM", 
            "agent_id": "VUANEM", 
            "sku": "1101006002001", 
            "product_src_img": "https://hi-static.fpt.vn/sys/hifpt/pnc_pdx/ftg-shopping/products/vuanem/1101006002001_1.jpg", 
            "product_supplier_name": "Goodnight", 
            "product_name": "Nệm bông ép Goodnight Nova", 
            "usual_price": 2850000, 
            "discount_price": 1710000, 
            "unit_type": null, 
            "product_active": 1, 
            "available_area": null, 
            "service_type": "PRODUCT", 
            "new": 0, 
            "quantity_current": 47, 
            "variants_tags": ["120x200"], 
            "info_agent": {
                "agent_id": "VUANEM", 
                "merchant_id": "VUANEM", 
                "service_type": "VN", 
                "agent_name": "Vua nệm", 
                "src_img": "https://hi-static.fpt.vn/sys/hifpt/pnc_pdx/ecom/agent/icon_Synnex_FPT.png", 
                "list_payment_method": "ONLINE", 
                "payment_methods": "INTERNATIONAL;DOMESTIC;WALLET_FOXPAY", 
                "description_error": "Hiện tại dịch vụ chỉ hỗ trợ online", 
                "invalid_locations": null
            }, 
            "could_option": 0, 
            "cart_id": 11947, 
            "quantity": 1, 
            "cart_active": 1
        }]
    }
}
```

### Application

![](./image/get_card_screen.jpg)

### **- add_cart**

### **action object**

Field | Data type| Required| Description
------|----------|---------|------------
type_request     | string   | YES     | Only input "add_cart"
data             | json     | YES     | Conditions
list_buying      | list     | YES     | List products buying Each value includes: product_id and quantity
product_id       | integer  | YES     | Product identification id
quantity         | integer  | YES     | Quantity of the product the user wants to buy
merchant_id      | string   | YES     | merchant_id is valid in [Merchant Partner](./Merchant_Partner.md)

### Sequence Diagram

![](./image/add_cart.jpg)

### Sample Request & Response

```json
{
    "data": {
        "list_buying": [{
            "product_id": 278, 
            "quantity": 1
        }], 
        "merchant_id": "VUANEM"
    },
    "type_request": "add_cart"
}
```

### Response

```json
{
    "status": 1, 
    "msg": "Đã thêm vào giỏ hàng", 
    "detail": null
}
```

### Application

![](./image/get_card_screen.jpg)

### **- edit_cart**

### **action object**

Field | Data type| Required| Description
------|----------|---------|------------
type_request     | string   | YES     | Only input "edit_cart"
data             | json     | YES     | Conditions
cart_id          | integer  | YES     | Id cart latest
quantity         | integer  | YES     | Product quantity
cart_active      | integer  | YES     | Only 1 or 0
merchant_id      | string   | YES     | merchant_id is valid in [Merchant Partner](./Merchant_Partner.md)

### Sequence Diagram

![](./image/edit_cart.jpg)

### Sample Request & Response

```json
{
    "data": {
        "cart_id": 11947, 
        "quantity": 4, 
        "cart_active": 1, 
        "merchant_id": "VUANEM"
    },
    "type_request": "edit_cart"
}
```

### Response

```json
{
    "status": 1, 
    "msg": "Thành công", 
    "detail": null
}
```

### Application

![](./image/edit_cart_screen.jpg)

### **- cal_prices**

### **action object**

Field | Data type| Required| Description
------|----------|---------|------------
type_request     | string   | YES     | Only input "cal_prices"
data             | json     | YES     | Conditions
cart_id          | list     | YES     | Must be list of integer
agent_id         | string   | YES     | Only "HIFPT"
promotion_code   | string   | NO      | Not Required. It can default ""

### Sequence Diagram

![](./image/cal_prices.jpg)

### Sample Request & Response

```json
{
    "data": {
        "cart_ids": [11951], 
        "agent_id": "HIFPT", 
        "promotion_code": ""
    },
    "type_request": "cal_prices"
}
```

### Response

```json
{
    "status": 1, 
    "msg": "OK", 
    "detail": {
        "total_raw_money": 600000, 
        "total_discount": 0.0, 
        "total_amount_finish": 600000.0, 
        "total_tickets": 3, 
        "skus": ["HIFPT2"], 
        "meta_data": {
            "events_skus": [{
                "sku": "HIFPT2", 
                "event_id": 104
            }]
        }, 
        "discounts": [{
            "discount_amount": 0.0, 
            "discount_type": "REDUCE_BY_TOTAL_UNITS", 
            "meta": {
                "104": {
                    "discount": 0, 
                    "total_prices": 600000, 
                    "total_ticket": 3, "discount_money": 0.0
                }
            }
        }, {
            "discount_amount": 0, 
            "discount_type": "VOUCHER", 
            "meta": null
        }], 
        "max_discount": 0
    }
}
```

### Application

![](./image/cal_prices_screen.jpg)