## **merchant_partners_orders**

## API: /hi-ecom-shopping-api/v1/web/merchant_partners/orders

## **a. General information**

#### **Sequence Diagram**

![](./image/merchant_partners_orders.jpg)

#### **call API(action)**

webview callback to app with API

#### **Request**

Params | Data type | Required | Description
-------|-----------|----------|------------
action | object    | YES      | webview callback to app with API
methods| POST      | YES      | webview callback to app with methods POST

#### **Action object**

Field | Data type| Required| Description
------|----------|---------|------------
action             | string   | YES     | Action needs to be processed by the main-app. posible values: "TOKEN", "User-Agent"
optional extra body| depends on the action type

#### **Action descriptions**

Action      | Description | Simple
------------|-------------|-------
getToken       | Get app's access token| {"TOKEN": "getToken"}
get User-Agent |                       | {"User-Agent":"get User-Agent"}


#### **Action with extra body**

Type request | Description | Extra params |Type   | Structure
-------------|-------------|--------------|-------|----
make   |  Call get_cart information| data| JSON | { "type_request": "make", "data": {...}}
complete  |  Add product to cart | data| JSON | { "type_request": "complete", "data": {...}}
cancel_order  |  Edit/Update cart (ignore merchant)| data| JSON | { "type_request": "cancel_order", "data": {...}}

## **b. Action object optional extra body**

### **- make**

### **action object**

Field | Data type| Required| Description
------|----------|---------|------------
type_request     | string   | YES     | Only input "make"
data             | json     | YES     | Conditions use get info
list_item        | list     | YES     | List integer
merchant_id      | string   | YES     | merchant_id is valid in [Merchant Partner](./Merchant_Partner.md)

### Sequence Diagram

![](./image/make.jpg)

### Sample Request & Response

```json
{
    "data": {
        "list_item": [11947, 12270], 
        "merchant_id": "VUANEM"
    }, 
    "type_request": "make"
}
```

### Response

```json
{
    "status": 1, 
    "msg": "Thành công", 
    "detail": {
        "trans_id": 9195, 
        "trans_type": "VUANEM", 
        "amount": 7139000, 
        "list_gift": [], 
        "info_fgold": {
            "point_of_customer": 700, 
            "value_of_point": 100, 
            "list_point_available": [100, 200, 300, 400, 500]
        }
    }
}
```

### Application

![](./image/make_screen.jpg)

### **- complete**

### **action object**

Field | Data type| Required| Description
------|----------|---------|------------
type_request     | string   | YES     | Only input "complete"
data             | json     | YES     | Conditions use get info
address_id       | integer  | YES     | Conditions use get info
is_exported_bill | integer  | YES     | Conditions use get info
note             | string   | NO      | Conditions use get info. It can default null
promotion_code   | integer  | NO      | Conditions use get info. It can default ""
referral_code    | integer  | NO      | Conditions use get info. It can default ""
trans_id         | string   | YES     | Conditions use get info
merchant_id      | string   | YES     | merchant_id is valid in [Merchant Partner](./Merchant_Partner.md)

### Sequence Diagram

![](./image/complete.jpg)

### Sample Request & Response

```json
{
    "data": {
        "address_id": 1524,
        "fgold": null,
        "is_exported_bill": 0,
        "merchant_id": "VUANEM",
        "note": null,
        "promotion_code": "",
        "referral_code": "",
        "trans_id": "9196"
    },
    "type_request": "complete"
}
```

### Response

### Application

![](./image/complete_screen.jpg)

### **- cancel_order**

### **action object**

Field | Data type| Required| Description
------|----------|---------|------------
type_request     | string   | YES     | Only input "cancel_order"
data             | json     | YES     | Conditions use get info
order_id_payment | string   | YES     | Conditions use get info
merchant_id      | string   | YES     | merchant_id is valid in [Merchant Partner](./Merchant_Partner.md)

### Sequence Diagram

![](./image/cancel_order.jpg)

### Sample Request & Response

```json
{
    "data": {
        "order_id_payment": "VN00006621", 
        "merchant_id": "VUANEM"
    }, 
    "type_request": "cancel_order"}
```

### Response

```json
{
    "status": 1, 
    "msg": "Thành công", 
    "detail": null}
```

### Application

![](./image/cancel_order_screen.jpg)