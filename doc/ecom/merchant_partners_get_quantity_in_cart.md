## **merchant_partners_get_quantity_in_cart**

## API: /hi-ecom-shopping-api/v1/web/merchant_partners/get-quantity-in-cart

## **a. General information**

#### **Sequence Diagram**

![](./image/merchant_partners_get_quantity_in_cart.jpg)

#### **call API(action)**

webview callback to app with API

#### **Request**

Params | Data type | Required | Description
-------|-----------|----------|------------
action | object    | YES      | webview callback to app with API
methods| POST      | YES      | webview callback to app with methods POST

#### **Action object**

Field | Data type| Required| Description
------|----------|---------|------------
action             | string   | YES     | Action needs to be processed by the main-app. posible values: "TOKEN", "User-Agent"
optional extra body| depends on the action type

#### **Action descriptions**

Action      | Description | Simple
------------|-------------|-------
getToken       | Get app's access token| {"TOKEN": "getToken"}
get User-Agent |                       | {"User-Agent":"get User-Agent"}

## **b. Action object optional extra body**

### **action object**

Field | Data type| Required| Description
------|----------|---------|------------
type_request     | string   | YES     | Only input "merchant_partners_get_quantity_in_cart"
data             | json     | YES     | Conditions use get info


### Sample Request & Response

```json

```

### Response

```json

```

### Application

![](./image/.jpg)