# Hi FPT super-app platform

Mini App is the solution to build sub-apps on Hi FPT's super-app platform to help users have seamless experiences without installing native apps or updating frequently.

## Outstanding numers

description| numbers
----|----
Hi FPT users | 3,000,000+
Monthly Active users| 100,000
Business partner | 20+
Payment method | Foxpay wallet, Domestic card accepted by more than 40 banks, International Visa/Master card

## Benefits for partner

- Approach 3 million Hi FPT users

- Cost-effectiveness

- Embedded into FPT ecosystem

## Comprehensive solutions for various industries

- Pickup Ordering

- Appointment Service Booking

- Ecommerce

## Functional Overview

![functional overview](doc/mo_hinh_hifpt_v03-overview.drawio.png)

## Hi FPT platform architect overview

![architect overview](doc/mo_hinh_hifpt_v03-architect_overview.drawio.png)


## Hi FPT microservices

![microservice](doc/mo_hinh_hifpt_v03-microservice.drawio.png)


## TMForum Open Digital Architect Reference design

![TFM ODA](doc/TMF_ODA_Ecom.png)
![TFM ODA](doc/TMF_ODA_ProductMgt.png)
![TFM ODA](doc/TMF_ODA_PartyMgt.png)
![TFM ODA](doc/TMF_ODA_Engagement.png)
![TFM ODA](doc/TMF_ODA_IntelligentMgt.png)


<!-- ## Mini-app Development process

![development process](doc/mo_hinh_hifpt_v03-mini-app_dev_process.drawio.png) -->


## Network Topology

![network topology](doc/hifpt_network_topo.jpg)



## Mini-app API Catalog

### User​

Name | Description
-----|------------
[requestUserConsents](doc/api/USER.md) | Request to access user’s information. It will show a bottom drawer to request for user information
[getUserConsents](doc/api/USER.md) | Get user info after permissions granted in requestUserConsents
[getUserAuth](doc/api/USER.md)| Get user authentication

### Permissions​

Name | Description
-----|------------
[requestPermission](doc/api/PERMISSIONS.md) | Request permissions on the device such as camera, location,...
[checkPermission](doc/api/PERMISSIONS.md) | Check permission has granted

### Location​

Name | Description
-----|------------
[getLocation](doc/api/LOCATION.md) | Get location of device

### Camera​

Name | Description
-----|------------
[scanQRCode](doc/api/CAMERA.md) | Get data from qr code

## Payment

Name | Description
-----|------------
[payment](doc/api/PAYMENT.md) | Call payment service


### Image​

Name | Description
-----|------------
[getBase64FromUrl](doc/api/IMAGE.md)| Convert image from url to base64 string
[getImage](doc/api/IMAGE.md)| Get image from gallery or capture new image
[saveImage](doc/api/IMAGE.md)| Save image to gallery
[getImageSize](doc/api/IMAGE.md)| Get size image

### Storage​

Name | Description
-----|------------
[getItem](doc/api/STORAGE.md) | Get data from storage
[setItem](doc/api/STORAGE.md) | Set data to storage
removeItem| Remove data from storage

### Navigation​

Name | Description
-----|------------
[goHome](doc/api/NAVIGATION.md) | Navigate to home screen
[goBack](doc/api/NAVIGATION.md) | Back to previous screen
[openUrl](doc/api/NAVIGATION.md) | Open url with callback deep link
[openWeb](doc/api/NAVIGATION.md) | Open web url
[dismissAll](doc/api/NAVIGATION.md) | Dismiss all screen
[dismiss](doc/api/NAVIGATION.md)| Dismiss current screen and return result for previous route

### Utilities​

Name | Description
-----|------------
[copyToClipboard](doc/api/UTILITIES.md) | Copy text to clipboard and show toast
[share](doc/api/UTILITIES.md) | Share text content to other apps

### Phone​

Name | Description
-----|------------
[openDialer](doc/api/PHONE.md) | Open dialer to call

### Message, Dialog​

Name | Description
-----|------------
[showToast](doc/api/MESSAGE.md) | Show toast
[hideToast](doc/api/MESSAGE.md) | Hide toast
[showAlert](doc/api/MESSAGE.md) | Show dialog alert
[showAction](doc/api/MESSAGE.md) | Show bottomsheet to choose action

### Screen​

Name | Description
-----|------------
[showLoading](doc/api/SCREEN.md) | Show loading
[hideLoading](doc/api/SCREEN.md) | Hide loading
